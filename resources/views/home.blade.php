@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Realizar Pedido</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('warning'))
                        <div class="alert alert-warning" role="alert">
                            {{ session('warning') }}
                        </div>
                    @endif

                    @if (session('danger'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('danger') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('upload') }}" enctype="multipart/form-data" style="height: 100%; width: 100%;">
                        @csrf
                        <div class="custom-file">
                            <input name="file" type="file" class="custom-file-input" id="customFile" lang="es">
                            <label class="custom-file-label" for="customFile">Buscar PDF ...</label>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1"></label>
                            <textarea name="about" required placeholder="Dejar Comentario" style="resize: none;" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <div class="form-group d-flex justify-content-end" style="margin-bottom: 0px;">
                            <button type="submit" class="btn btn-primary">
                                Subir Archivo
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            @if($user->admin)
                <br>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Todos los Pedidos</h5>
                        <h6 class="card-subtitle mb-2 text-muted" style="margin-bottom: 0px !important;">Vista de Administrador</h6>
                    </div>
                </div>
                @forelse ($posts as $post)
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <a target="_blank" rel="noopener noreferrer" href="{{ Storage::url($post->file) }}"><strong>{{ $post->originalFile }}</strong></a>
                            <hr>
                            <div class="d-flex justify-content-between">
                                <div class="col-md-8">
                                    @if($post->user->admin)
                                        <span class="badge badge-primary">Administrador</span>
                                    @endif
                                    @if($post->user->isOnline())
                                        <span class="badge badge-success">Disponible</span>
                                    @else
                                        <span class="badge badge-danger">No Disponible</span>
                                    @endif
                                    @if($post->user->id == Auth::id())
                                        <span class="badge badge-dark">Mi Cuenta</span>
                                    @endif
                                    <br>
                                    <span class="font-weight-bold">{{ $post->user->name }}</span>
                                    <br>
                                    <span class="font-weight-light">{{ $post->user->email }}</span>
                                    <br>
                                    <span class="font-weight-light">{{ $post->about }}</span>
                                </div>
                                <div class="col-md-4 d-flex justify-content-end">
                                    <span class="font-italic">{{ (new Carbon\Carbon($post->created_at))->diffForHumans() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <br>
                    <div class="card">
                        <div class="card-body">
                            No hay Archivos
                        </div>
                    </div>
                @endforelse
            @else
                <br>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Mis Pedidos</h5>
                        <h6 class="card-subtitle mb-2 text-muted" style="margin-bottom: 0px !important;">Cuenta Personal</h6>
                    </div>
                </div>
                @forelse ($posts as $post)
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <a target="_blank" rel="noopener noreferrer" href="{{ Storage::url($post->file) }}"><strong>{{ $post->originalFile }}</strong></a>
                            <hr>
                            <div class="d-flex justify-content-between">
                                <div class="col-md-8">
                                    <span class="font-weight-light">{{ $post->about }}</span>
                                </div>
                                <div class="col-md-4 d-flex justify-content-end">
                                    <span class="font-italic">{{ (new Carbon\Carbon($post->created_at))->diffForHumans() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <br>
                    <div class="card">
                        <div class="card-body">
                            No hay Archivos
                        </div>
                    </div>
                @endforelse
            @endif
        </div>
    </div>
</div>
@endsection
