<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div class="card">
	    <div class="card-body">
            <h5 class="card-title">Nuevo Pedido</h5>
            <h6 class="card-subtitle mb-2 text-muted">codigo: {{ $post->id }}</h6>
	        <a target="_blank" rel="noopener noreferrer" href="{{Request::root() . Storage::url($post->file) }}"><strong>{{ $post->originalFile }}</strong></a>
	        <hr>
	        <div class="d-flex justify-content-between">
	            <div class="col-md-8">
	                @if($post->user->admin)
	                    <span class="badge badge-primary">Administrador</span>
	                @endif
	                @if($post->user->isOnline())
	                    <span class="badge badge-success">Disponible</span>
	                @else
	                    <span class="badge badge-danger">No Disponible</span>
	                @endif
	                @if($post->user->id == Auth::id())
	                    <span class="badge badge-dark">Mi Cuenta</span>
	                @endif
	                <br>
	                <span class="font-weight-bold">{{ $post->user->name }}</span>
	                <br>
	                <span class="font-weight-light">{{ $post->user->email }}</span>
	                <br>
	                <span class="font-weight-light">{{ $post->about }}</span>
	            </div>
	            <div class="col-md-4 d-flex justify-content-end">
	                <span class="font-italic">{{ (new Carbon\Carbon($post->created_at))->diffForHumans() }}</span>
	            </div>
	        </div>
	    </div>
	</div>
</body>
</html>
