<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file', 'about','user_id','originalFile'
    ];

    /**
     * Get the user method record associated with the reservation.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
