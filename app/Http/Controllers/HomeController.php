<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\User;
use Carbon\Carbon;
use Cache;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendConfirmation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->admin) {
            $posts = Post::All();
            return view('home', [
                'posts' => $posts->sortByDesc('id'),
                'user'  => $user
            ]);
        } else {
            return view('home', [
                'posts' => $user->posts->sortByDesc('id'),
                'user'  => $user
            ]);
        }
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            $post = Post::create([
                'file'         => $request->file->store('public/files'),
                'about'        => $request->about,
                'user_id'      => Auth::id(),
                'originalFile' => $request->file->getClientOriginalName()
            ]);
        }

        $admins = User::Where('admin',true)->get();
        foreach ($admins as $admin) {
            Mail::to($admin->email)->send(new SendConfirmation($post));
        }

        return redirect()->back()->with('status', 'archivo subido!');
    }

    public function admin(Request $request)
    {
        $user = Auth::user();
        $user->admin = !$user->admin;
        $user->save();

        $message = ($user->admin) ? "Ahora eres administrador !" : "Ya no eres administrador!";
        $status = ($user->admin) ? "status" : "warning";

        return redirect()->back()->with($status, $message);
    }

}
